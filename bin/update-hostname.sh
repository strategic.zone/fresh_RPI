#!/usr/bin/env bash
#title		:update-hostname.sh
#description	:update hostname with MAC address
#author		:Valeriu Stinca
#email		:ts@strategic.zone
#date		:20180331
#version	:1.5
#notes		:For Raspberry PI
#required	:net-tools, add 'net.ifnames=0' to cmdline.txt (disable Predictable Network Interface Names)
#===================

PREFIX_HOSTNAME="${1}"
CURRENT_HOSTNAME=$(cat /proc/sys/kernel/hostname)
if ! command -v ifconfig
then
	echo "Command ifconfig not found, please install net-tools package with apt install net-tools"
	exit 1
fi
MAC=$(ifconfig -a eth0 | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}' | sed 's/://g')
# MAC=$(cat /sys/class/net/eth0/address | sed 's/://g')
NEW_HOSTNAME="${PREFIX_HOSTNAME}-${MAC:(-6)}"
if [[ "${CURRENT_HOSTNAME}" != "${NEW_HOSTNAME}" ]]
then
    if [[ -d /mnt/root-ro ]]
    then
        MY_ROOT="/mnt/root-ro/"
        /bin/mount -o remount,rw /mnt/root-ro
    else
        MY_ROOT="/"
        /bin/mount -o remount,rw /
    fi
    echo "${NEW_HOSTNAME}" | tee "${MY_ROOT}etc/hostname"
    /bin/sed -i "/127.0.1.1*/c\127.0.1.1\t${NEW_HOSTNAME}" "${MY_ROOT}etc/hosts"
    /usr/bin/hostnamectl set-hostname "${NEW_HOSTNAME}"

: <<'END'
    # add hostapt name from hostname
    if [[ -f /etc/hostapd/hostapd.conf ]]
    then
        /bin/sed -i "/^ssid*/c\ssid\=${NEW_HOSTNAME}" "${MY_ROOT}etc/hostapd/hostapd.conf"
        /bin/sed -i "/^wpa_passphrase*/c\wpa_passphrase\=${MAC:(-6)}-${MAC:(-6)}" "${MY_ROOT}etc/hostapd/hostapd.conf"
    fi
END

    #sync and reboot
    /bin/sync
    /sbin/reboot
fi
